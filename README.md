# RepliSim

This is an event-based simulator developed to support a research on data management for scientific workflows. Data to be analyzed is present at a centralized storage device. Analysis tasks are submitted to a framework, that will schedule them to run in processing nodes of a cluster. The framework also manages a part of the storage capacity of the processing nodes, used as a cache for files.
The aim of the simulator is to evaluate the impact of different replication and caching strategies in the analysis tasks' performance.

## How to run

python simulation.py [parameters file]

If a parameters file is not provided, default values (defined in sim_input.py) are used.
Refer to example.param for an example of parameters file.

## Limitations

For simplicity, network is modeled as its bandwidth, contention and the topology are ignored. Each task is assumed to be sequential (to run in a single processing node), to access a single input file, and to output nothing. Tasks performance depends only on the data access speed.

## Known issues

The usage of the processing nodes by external jobs is not working as expected.

## Details of the simulation

In the simulation there are P identical processing nodes with a given local storage capacity used for the cache. F files are created (and possibly cached) before the beginning of the simulation. Each file has a number of times it will be used, so a number of tasks is created and queued accordingly. They don't arrive during the simulation, but rather are already queued at the beginning. The tasks will be executed following a FIFO policy.

Initial caching and replication are instantaneous and are assumed to be already in place when the simulation time starts.

### File creation 

File sizes are generated following a normal distribution with given average and standard deviation. 
Each file has a reuse factor which is the number of times the file will be used. They are generated following an exponential distribution and then added 1 (because our minimum is 1, not 0).
The first file will have the first drawn value from the first distribution as size and the first drawn value from the second distribution as reuse factor, the second will have the second values, and so on.

### Initial caching

Before starting the simulation we place all the files in the processing nodes cache. We iterate through the files and assign each of them to the server with the least amount of stored data at that moment. If the total dataset size is larger than what is available for cache, there will be evictions during this process. 
When using the "nodes" replication strategy, at this moment we write to only the (1-replication_quota)*P first processing nodes.

### Replication

Before starting the simulation some replicas are created according to the configured replication quota. These decisions are made considering a score of each file, calculated using the number of accesses that file received. At the very beginning, no files were accessed, so they all have the same score and decisions are basically random.

The file score is calculated as the number of accesses divided by the number of existing replicas at a given moment. The score of a node is the sum of 1+score of the files it caches. 

In the "nodes" replication strategy, we take the replication_quota*P nodes with highest scores and make replicas of them in the last replication_quota*P nodes (which did not receive files at the beginning of the simulation). In this strategy, each node replicates the whole contents of another node.

### Simulation loop

These steps will be executed until all queued tasks finished their execution.

#### Schedule tasks

While there are tasks to run and available nodes, schedule tasks (FIFO, so simply take the first from the list of tasks). 
The process of choosing a node to run the task goes as follows:

- Go through the available nodes and stop at the first that contains the file for the task. If we found one, that is where the task will run.
- If we did not find a node containing the file, we take the first from the list of available nodes.

We calculate the time it will take to run the task based on the file size and the bandwidth to access it locally or remotely (provided as arguments), then we register the event of the end of this task for current_clock+duration.

#### Fast forward to the next event

If there are no tasks to run at this point we fast forward to the next event, which can be:

##### A task finished its execution

We release the node but first if the access was not to a local file (it came from the centralized storage) we add the new file to the node's local storage (that does not take time because that already happened while the task executed). 

##### Periodically re-take replication decisions

When this event takes place, we re-calculate all scores and re-do the decision of what nodes/files to replicate as in item 3 above, but now instead of immediately writing the files to the replica nodes, we create transfer between processing nodes and replica nodes (to copy the files). 

The end of these transfers are events that will happen at a future time (their duration is obtained using a parameter for the speed of transfer between processing nodes). That means that new replicated files will NOT be available for tasks until they were transferred. 

If writing to a cache where there is not enough space, some files will be evicted from it according a cache replacement policy. For now, we evict the files with the lowest score of that node.

##### End of a file transfer between local processing nodes

One thing to notice about the file transfers is that nodes continue to be available to run tasks while transferring files, and the performance of these tasks is not affected by it. That is a simplification of this simulation. 

### Node dynamicity

Every time a node is freed after being used to run a processing task or performing a file transfer, that node has a probability of being taken by external users of the cluster, and thus becoming completely unavailable to the framework for a period of time.

### End of simulation

The simulator reports a number of metrics, including:

- simulated time;
- amount of data written per node to its local storage;
- amount of data erased per node from its local storage;
- performance for each task (relative to local access, so if the file was local to the task its performance is 1.0, and if file was remote the task performance is bandwidth_remote/bandwidth_local);
- usage of the interconnection between the centralized storage and the cluster (a list of timestamps where something changes - namely because a task doing remote access started or finished - and corresponding bandwidth).

## Code organization

- simulation.py is the main source file, with the simulation initialization and loop.
- sim_input.py contains the SimulationParameters class, with default values for the parameters of the simulation, and the function to read them from a given file.
- event.py has classes for the different event types (all extend the EventInfo one), and the EventList class that is used during the simulation to keep track of events. The latter provides the push and pop methods to respectively register a future event and to fast forward to the next one.
- sim_output.py contains the SimulationOutput class definition, that is used during the simulation to keep track of metrics and to report them at the end.
- replica.py contains implementations for different replication strategies, all extend the ReplicaStrategy class and provide the initial_replicas and review methods.
- cache_replacement.py contains implementations for the different cache replacement strategies, all extend the CacheReplacementStrategy class and provide the replace method.
- node.py contains the processing nodes. The Cluster class is used to refer to the whole set of processing nodes, and implements the external jobs. The Node class is used to keep information to each processing node and perform operations like adding or removing files.
- sim_file.py contains the File class, used to keep information to each file present in the simulation, and the FileStore class, used to create and initially distribute the whole set of files according to input parameters.
- task.py contains the Task class, used to keep information about a processing task, and the FIFOTaskManager class, which implements the task scheduler of the framework.
 
