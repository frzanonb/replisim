import heapq
import numpy as np
from sim_output import SimulationOutput
from transfer import TransferInfo


class ReplicaStrategy():
	"""
	Generic replication strategy class, NOT to be used directly but
	to be the parent of different strategies implementations.

	...

	Attributes
	----------
	name : str
		the name of the policy being used, provided by the
		initialization of the class implementing that policy.
	output : SimulationOutput
		a reference to the output object that keeps metrics to
		be reported at the end of the simulation
	filestore : FileStore
		a reference to the FileStore object which contains the
		information about all files.

	Methods
	-------
	calculate_node_scores(nodes)
		Calculate the scores of all nodes in the system,
		assuming the scores of the files were already
		calculated and normalized earlier. No values are
		returned, they are only updated.
	initial_replicas(files, node_info, nodes)
		Called to make the first replicas at the beginning of
		the simulation. The child classes MUST implement this.
	review(files, node_info)
		Called periodically during the simulation to review the
		decisions on replicas. Returns a list of transfers to
		be made to respect the replication decision.
	"""
	def __init__(self, name, output, filestore):
		"""
		Parameters
		----------
		name : str
			what replica strategy we are using
		output : SimulationOutput
			a reference to the output object that keeps metrics to
			be reported at the end of the simulation
		filestore : FileStore
			a reference to the FileStore object which contains the
			information about all files.
		"""
		self.name = name
		self.output = output
		self.filestore = filestore

	def calculate_node_scores(self, nodes):
		"""
		Calculate the scores of all nodes in the system,
		assuming the scores of the files were already
		calculated and normalized earlier. No values are
		returned, they are only updated.

		Parameters
		----------
		nodes : List
			a list of Node objects containing all nodes
			in the cluster.
		"""
		for n in range(len(nodes)):
			nodes[n].calculate_score()
			# we don't have to normalize it because node
			# scores are the average of file scores, which
			# are values between 0 and 1
			assert ((nodes[n].score >= 0.0) and
				(nodes[n].score <= 1.0))

	def initial_replicas(self, files, node_info, nodes):
		"""
		Called to make the first replicas at the beginning of
		the simulation. The child classes MUST implement this.

		Parameters
		----------
		files : List
			the list of File objects for all files in the
			simulation.
		node_info : List
			the list of Node objects for all nodes in the
			simulation.
		nodes : int
			the number of nodes that are already caching
			files, len(node_info)-nodes is the number of
			replicas we'll do (to the remaining nodes)

		Raises
		------
		NotImplementedError
			if called with the parent class, all children
			MUST implement this.
		"""
		raise NotImplementedError

	def review(self, files, node_info):
		"""
		Called periodically during the simulation to review the
		decisions on replicas. Returns a list of transfers to
		be made to respect the replication decision.

		Parameters
		----------
		files : List
			the list of File objects for all files in the
			simulation.
		node_info : List
			the list of Node objects for all nodes in the
			simulation.

		Returns
		-------
		List
			A list of TransferInfo objects, with all the
			transfers to be made to respect the
			replication decision.

		Raises
		------
		NotImplementedError
			if called with the parent class, all children
			MUST implement this.
		"""
		raise NotImplementedError


class NodeReplicaStrategy(ReplicaStrategy):
	"""
	The "nodes" replication strategy. Whole nodes are replicated
	(the top scores).

	...

	Attributes
	----------
	replicas : Dict
		a mapping of nodes to what other nodes they are
		currently replicating.

	Methods
	-------
	replicate(files, node_info)
		Used every time we make decisions on replicas (called
		by review). It will update the replicas dictionary.
	simple_replicate_node(source, dest)
		Copies all the contents of source to dest. Used only in
		the first decision on replicas, before the start of the
		simulation,  because it does not generate transfers.
	"""
	def __init__(self, output, filestore):
		ReplicaStrategy.__init__(self, "nodes", output, filestore)
		self.replicas = {}

	def initial_replicas(self, files, node_info, nodes):
		# put the nodes that receive replicas in our mapping
		for i in range(nodes, len(node_info)):
			self.replicas[i] = -1
		# make the decisions of what node will replicate what
		# other node
		self.replicate(files, node_info)
		# do the replicas right away
		for dest in self.replicas:
			self.simple_replicate_node(node_info[self.replicas[dest]],
							node_info[dest])

	def review(self, files, node_info):
		# make the decisions
		review_log = self.replicate(files, node_info)
		# now list the required transfers
		transfers = []
		for dest in self.replicas:
			source = self.replicas[dest]
			for i in node_info[source].myfiles:
				if not (i in node_info[dest].myfiles):
					transfers.append(TransferInfo(node_info[source],
								node_info[dest],
								i))
		return (transfers, review_log)

	def replicate(self, files, node_info):
		"""
		Used every time we make decisions on replicas (called
		by review). It will update the replicas dictionary.

		Parameters
		----------
		files : List
			the list of File objects for all files in the
			simulation.
		node_info : List
			the list of Node objects for all nodes in the
			simulation.

		Returns
		-------
		str
			a debug string about this operation
		"""
		review_log = ""
		# calculate all scores and make a priority queue (a
		# heap) to get the max scores easily
		max_file_score = self.filestore.calculate_file_scores()
		node_scores = []
		for i in range(len(node_info)):
			if not (i in self.replicas):
				# not all nodes, exclude the replicas
				node_info[i].calculate_score()
				# -score because the priority queue
				# will give the lowest value and we
				# want the highest
				node_scores.append((-node_info[i].score, i))
		heapq.heapify(node_scores)
		# replicate the top scores (limited by the number of
		# nodes we have to store replicas)
		buf = []
		for i in self.replicas:
			# if this node is not available for us at this
			# moment (because it is running an external
			# job), we don't change the decision to it
			if node_info[i].external:
				if review_log != "":
					review_log += "\n"
				review_log += ("won't change decisions for node " +
						str(i) +
						" because it is not available")
				continue
			# get the node with the highest score
			if len(node_scores) == 0:
				# we ran out of nodes, start over
				for elem in buf:
					heapq.heappush(elem)
				buf = []
			pair = heapq.heappop(node_scores)
			source = pair[1]
			buf.append(pair)
			while (node_info[source].external and
				(source != self.replicas[i]) and
				(len(node_scores) > 0)):
				source = heapq.heappop(node_scores)[1]
			if ((len(node_scores) == 0) and
				node_info[source].external and
				(source != self.replicas[i])):
				# we ran out of nodes while searching
				# nothing will change
				break
			if(source != self.replicas[i]):
				# it is possible this node was
				# previously set to replicate a
				# different node than we've just
				# decided it will replicate
				if review_log != "":
					review_log += "\n"
				review_log += ("the node " + str(i) +
						" was a replica of " +
						str(self.replicas[i]) +
						" but now it will replicate " +
						str(source))
				# else we are not replicating yet, the
				# function calling this one will make a
				# list of transfers.
				# update our mapping of replica nodes
				# to what they are replicating
				self.replicas[i] = source
		return review_log

	def simple_replicate_node(self, source, dest):
		"""
		Copies all the contents of source to dest. Used only in
		the first decision on replicas, before the start of the
		simulation,  because it does not generate transfers.

		Parameters
		----------
		source : Node
			node that will be replicated
		dest : Node
			node that will store a replica of source
		"""
		for i in range(len(source.myfiles)):
			dest.write(source.myfiles[i])


class ObjectReplicaStrategy(ReplicaStrategy):
	"""
	The "objects" replication strategy. Objects (files) are
	replicated to other nodes according to their scores.

	...

	Attributes
	----------
	param : SimulationParameters
		a reference to the simulation parameters.

	Methods
	-------
	replicate(files, node_info)
		Used every time we make decisions on replicas (called
		by review). Takes the files with highest scores and
		places new replicas of them in nodes with lowest
		scores.
	"""
	def __init__(self, output, filestore, param):
		"""
		Parameters
		----------
		param : SimulationParameters
			a reference to the simulation parameters.
		"""
		ReplicaStrategy.__init__(self, "objects", output, filestore)
		self.param = param

	def initial_replicas(self, files, node_info, nodes):
		# nodes is an unused argument (the nodes replica
		# strategy uses it).
		# make the decisions and generate a list of replicas to
		# be made
		newreplicas, replicate_log = self.replicate(files, node_info)
		# here we simply write the files to their destination
		# nodes instead of generating transfers because we are
		# doing the initial setup
		for r in newreplicas:
			r[1].write(r[0])
		return replicate_log

	def review(self, files, node_info):
		transfers = []
		# make the decisions and generate a list of replicas
		# to be made
		newreplicas, replicate_log = self.replicate(files, node_info)
		# now make a list of transfers
		for r in newreplicas:
			# find a node from where to make the copy (from
			# the nodes that store this file).
			# IMPORTANT: here we are taking the first
			# available node with no regard to load balancing
			found = False
			for source in r[0].dest:
				if not source.external:
					found = True
					break
			# the replicate function checks for this, so we
			# should not have this problem, but adding the
			# assert to be sure
			assert found
			transfers.append(TransferInfo(source, r[1], r[0]))
		return (transfers, replicate_log)

	def replicate(self, files, node_info):
		"""
		Used every time we make decisions on replicas (called
		by review). Takes the files with highest scores and
		places new replicas of them in nodes with lowest
		scores.

		Parameters
		----------
		files : List
			the list of File objects for all files in the
			simulation.
		node_info : List
			the list of Node objects for all nodes in the
			simulation.

		Returns
		-------
		List
			a list of replicas to be made. Each element is
			a (File, Node) pair.
		str
			a debug string about this operation
		"""
		replicate_log = ""
		# we will use two heaps for this, one for files, where
		# at each access we will get the file with highest
		# score, and another for nodes, where at each access we
		# will get the node with lowest score
		file_scores = []
		node_scores = []
		max_file_score = self.filestore.calculate_file_scores()
		for i in range(len(files)):
			# we use -score because we want to always get
			# the highest, and the minheap will give us the
			# lowest
			file_scores.append((-files[i].score, i))
		ReplicaStrategy.calculate_node_scores(self, node_info)
		for i in range(len(node_info)):
			# here we use the score (and NOT -score)
			# because we always want the lowest score
			node_scores.append((node_info[i].score, i))
		heapq.heapify(file_scores)
		heapq.heapify(node_scores)
		# we will need to keep track of changes that need to be
		# made to file and node scores as they change with
		# replication decisions
		file_new_dests = {}
		for i in range(len(files)):
			file_new_dests[i] = 0
		node_new_files = {}
		for i in range(len(node_info)):
			node_new_files[i] = []
		# now we do a certain number of replicas
		newreplicas = []
		# IMPORTANT we are making decisions based only on the
		# number of files, not on their sizes
		while ((len(newreplicas) < (self.param.f*self.param.replica_quota)) and
			(len(file_scores) > 0)):
			# replicate the file with highest score
			pair = heapq.heappop(file_scores)
			file_info = files[pair[1]]
			# check if we can find this file in a node that
			# is available, otherwise it is impossible to
			# replicate it at this moment
			available = False
			for source in file_info.dest:
				if not source.external:
					available = True
					break
			if not available:
				# it is impossible to find this file
				# somewhere to replicate it, so we
				# skip it
				if replicate_log != "":
					replicate_log += "\n"
				replicate_log += ("File " + str(file_info.id) +
						" has the highest score " +
						str(file_info.score) +
						" but it cannot be replicated \
						because no available node has it.")
				continue
				# IMPORTANT we are choosing to skip
				# this file, we could instead bring
				# it from the centralized storage...
			# to the node with the lowest score (but we
			# need to skip nodes that are currently not
			# available and the ones that already have
			# copies of this file)
			if(len(node_scores) == 0):
				if replicate_log != "":
					replicate_log += "\n"
				replicate_log += "We ran out of available nodes to store replicas"
				break
			node = node_info[heapq.heappop(node_scores)[1]]
			buf = []
			while ((node.external or
				(file_info in node.myfiles) or
				((file_info, node) in newreplicas))
				and (len(node_scores) > 0)):
				# this node is not appropriate for this
				# file
				if not node.external:
					# but it isn't so because it
					# already has this file, it
					# could be a good choice for
					# another file
					buf.append(node)
				node = node_info[heapq.heappop(node_scores)[1]]
			failed = False
			if ((len(node_scores) == 0) and
				(node.external or
					(file_info in node.myfiles) or
					((file_info, node) in newreplicas))):
				# we ran out of nodes and could not
				# find where to place a new replica of
				# this one, so we simply skip it.
				# before skipping we need to execute
				# the next instructions to re-add the
				# skipped nodes to the heap
				failed = True
			for n in buf:
				# put the available nodes back to the
				# heap, they could be used for another
				# file
				heapq.heappush(node_scores, (n.score, n.id))
			if failed:
				if replicate_log != "":
					replicate_log += "\n"
				replicate_log += ("File " + str(file_info.id) +
						" has the highest score " +
						str(file_info.score) +
						" but it cannot be replicated \
						because every node either has \
						it or is being used by an \
						external job.")
				continue
			# ok, we have a file to be replicated and we
			# know where to place it
			newreplicas.append((file_info, node))
			if replicate_log != "":
				replicate_log += "\n"
			replicate_log += ("File " + str(file_info.id) +
					" has the highest score " +
					str(file_info.score) +
					" and is going to be replicated to node " +
					str(node.id))
			# we recalculate the file score and put it back
			# in the heap because we could be interested in
			# creating multiple copies of the same file
			file_new_dests[pair[1]] += 1
			assert not (file_info in node_new_files[node.id])
			node_new_files[node.id].append(file_info)
			file_info.calculate_raw_score(file_new_dests[pair[1]])
			if file_info.raw_score > max_file_score:
				max_file_score = file_info.raw_score
				# we will have to re-normalize the
				# score of all files because the
				# maximum value has changed
				for f in files:
					f.normalize_score(file_info.raw_score)
				# now we need to redo the heap of file
				# scores because the values have
				# changed.
				for f in range(len(file_scores)):
					# update the score of the file
					# in the heap data structure
					file_scores[f][0] = -files[file_scores[f][1]].score
				# redo the heap because we changed the
				# values without respecting the heap
				# structure
				heapq.heapify(file_scores)
				# and also we have to recalculate all
				# nodes scores because the scores of
				# the files have changed
				for i in range(len(node_info)):
					node_info[i].calculate_score(node_new_files[i])
				# and finally we have to redo the heap
				# of node scores because the values
				# have changed
				for n in range(len(node_scores)):
					node_scores[n][0] = node_info[node_scores[n][1]].score
				heapq.heapify(node_scores)
			else:
				# since the maximum has not changed, we
				# don't have to change the scores of
				# other files or nodes
				file_info.normalize_score(max_file_score)
				node.calculate_score(node_new_files[node.id])
			if(file_info.score != 0.0):
				# if this file's score is 0.0 and it's
				# still the highest, all files have
				# score 0, so no point in re-adding
				# this one and risking having only
				# replicas of it
				heapq.heappush(file_scores,
					(-file_info.score, file_info.id))
			# we recalculate the node score and put it back
			# in the heap
			heapq.heappush(node_scores, (node.score, node.id))
		return (newreplicas, replicate_log)
