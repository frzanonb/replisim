from sim_input import SimulationParameters
from sim_output import SimulationOutput


class Node:
	"""
	A node from the cluster where files can be stored (it contains
	a certain storage capacity) and where tasks run.

	...

	Attributes
	----------
	id : int
		my node identifier
	myfiles : List
		list of files (File object) stored in this node's local
		storage device
	output : SimulationOutput
		a reference to the output object that keeps metrics to
		be reported at the end of the simulation
	param : SimulationOutput
		a reference to the simulation parameters object  end of
		the simulation
	replace : CacheReplacementStrategy
		a reference to the cache replacemnt policy in use
	volume : int
		total amount of data stored in this node in MB
	score : Float
		the score of this node, used for replication decisions.
		BEWARE: this is not necessarily up to date at all
		times, the calculate_score function is only called when
		doing replication decisions.
	transfers : List
		list of ongoing transfers from or to this node
	external : bool
		set to True when this node is running an external job

	Methods
	-------
	write(file)
		Write a file to this node's local storage device
	calculate_score(newfiles=[])
		Calculate the score for this node which is the average
		of its files' scores. It is updated, not returned.
		This function assumes the file scores were already
		calculated.
	print()
		Returns a string to be printed with all information
		about this node.
	remove(file)
		Remove a file from this local storage device.
	transfer(transfer)
		Called to register an ongoing transfer of a given file
		from this node to a destination node.
		Important: this function will do everything that is
		needed for both nodes, so it is to be called only once
		(for the source).
	end_transfer(transfer)
		Called to register the end of a transfer from this node
		to a destination node.
		Important: this function will do everything that is
		needed for both nodes, so it is to be called only once
		(for the source).
	"""
	def __init__(self, identifier, output, param, replace):
		"""
		Parameters
		----------
		identifier : int
			my node identifier
		output : SimulationOutput
			a reference to the output object that keeps metrics to
			be reported at the end of the simulation
		param : SimulationOutput
			a reference to the simulation parameters object  end of
			the simulation
		replace : CacheReplacementStrategy
			a reference to the cache replacemnt policy in use
		"""
		self.id = identifier
		self.myfiles = []
		self.output = output
		self.param = param
		self.replace = replace
		self.volume = 0
		self.score = 0.0
		self.transfers = []
		self.external = False

	def write(self, file_info):
		"""
		Write a new file to this node's local device

		Parameters
		----------
		file_info : File
			file to be written
		"""
		if self.param.debug:
			print("WRITE " + str(file_info.id) +
				" to " + str(self.id))
		assert not (file_info in self.myfiles)
		# check if we have the capacity to store it, and if not
		# remove some files
		if (self.param.stor_capacity - self.volume) < file_info.size:
			self.replace.replace(self, file_info.size)
		# now we have the space, we can write
		self.myfiles.append(file_info)
		self.volume += file_info.size
		file_info.written(self)
		self.output.written(self.id, file_info.size)
		self.output.new_capacity(self)

	def calculate_score(self, newfiles=[]):
		"""
		Calculate the score for this node which is the average
		of its files' scores. It is updated, not returned.
		This function assumes the file scores were already
		calculated.

		Parameters
		----------
		newfiles : List (optional)
			A list of files that are NOT stored by this
			node, but that we'll consider are stored in the
			node to calculate the score. This is used when
			making decisions about replications (we want to
			know what will be the score of nodes after
			the new decisions are implemented).
		"""
		self.score = 0.0
		for i in self.myfiles:
			assert (i.score >= 0.0) and (i.score <= 1.0)
			self.score += i.score
		for newfile in newfiles:
			assert ((newfile.score >= 0.0) and
				(newfile.score <= 1.0))
			self.score += newfile.score
		if (len(self.myfiles)+len(newfiles)) > 0:
			self.score = (self.score /
					float(len(self.myfiles)+len(newfiles)))

	def print(self):
		"""
		Returns a string to be printed with all information
		about this node.
		"""
		ret = ("Node " + str(self.id) + " has " +
			str(len(self.myfiles)) + " files for a total of " +
			str(self.volume) + " MB. My current score is " +
			str(self.score) + " and my files are: ")
		for i in self.myfiles:
			ret += str(i.id)
			if i != self.myfiles[len(self.myfiles)-1]:
				ret += ", "
		return ret

	def remove(self, file_info):
		"""
		Remove a file from this local storage device.

		Parameters
		----------
		file_info : File
			the File object of the file we are removing
		"""
		if self.param.debug:
			print("REMOVE " + str(file_info.id) +
				" from " + str(self.id))
		assert file_info in self.myfiles
		self.myfiles.remove(file_info)
		assert not (file_info in self.myfiles)
		self.volume -= file_info.size
		file_info.removed(self)
		self.output.removed(self.id, file_info.size)

	def __lt__(self, other):
		"""
		We override the < comparator, used to break ties in the
		score heap (it defines how to choose between nodes with
		the same score, in that case we use the id)

		Parameters
		----------
		other : Node
			the other object we want to compare to this one

		Returns
		-------
		bool
			is this object smaller than the other object?
		"""
		if self.score == other.score:
			return self.id < other.id
		else:
			return self.score < other.score

	def transfer(self, transfer):
		"""
		Called to register an ongoing transfer of a given file
		from this node to a destination node.
		Important: this function will do everything that is
		needed for both nodes, so it is to be called only once
		(for the source).

		Parameters
		----------
		transfer : TransferInfo
			the TransferInfo object for this transfer
		"""
		self.transfers.append(transfer)
		transfer.dest.transfers.append(transfer)

	def end_transfer(self, transfer):
		"""
		Called to register the end of a transfer from this node
		to a destination node.
		Important: this function will do everything that is
		needed for both nodes, so it is to be called only once
		(for the source).

		Parameters
		----------
		transfer : TransferInfo
			the TransferInfo object for this transfer
		"""
		assert ((transfer in self.transfers) and
			(transfer in transfer.dest.transfers))
		self.transfers.remove(transfer)
		transfer.dest.transfers.remove(transfer)
