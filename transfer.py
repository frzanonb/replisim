class TransferInfo:
	"""
	Keeps information about a file being transferred between two
	processing nodes.

	...

	Attributes
	----------
	source : Node
		from where the file is being transferred
	dest : Node
		to where the file is being transferred
	file_info : File
		the file being transferred.
	"""
	def __init__(self, source, dest, file_info):
		"""
		Parameters
		----------
		source : Node
			from where the file is being transferred
		dest : Node
			to where the file is being transferred
		file_info : File
			the file being transferred.
		"""
		self.source = source
		self.dest = dest
		self.file = file_info
