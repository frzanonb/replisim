class SimulationParameters:
	"""
	Parameters for the simulation.
	WHEN ADDING a new simulation parameter below in one of the
	functions (for instance, in __init__), don't forget to do the
	same in the other functions (for instance, the print_str
	function).
	The object can be created providing or not values for the
	parameters (if they are not provided, the default values are
	used)

	...

	Attributes
	----------
	p : int
		number of processing nodes
	stor_capacity : int
		in MB, storage capacity of the processing nodes
		(the amount available for storing files)
	f : int
		number of files.
	file_size_avg : int
		average file size in MB. We assume a normal
		distribution for the file size, defined by its average
		and standard deviation.
	file_size_std : int
		standard deviation for file size in MB.
		We assume a normal
		distribution for the file size, defined by its average
		and standard deviation.
	file_reuse_avg : int
		average number of times each file is going to be
		re-used (if it is zero, then it is going to be used
		once). We assume an exponential distribution for the
		file reuse factor.
	replica_quota : Float
		part (between 0 and 1) of the total cache capacity
		being used to store replicas in the nodes strategy,
		fraction of the number of files to be replicated in
		the objects replication strategy
	replica_strategy : str
		the replication strategy. "nodes" sets aside
		replica_quota of the nodes to only store replicas.
		"objects" replicates replica_quora*100 % of the files
	local_bandw : int
		bandwidth in MB/s when accessing a file that is stored
		in the node where the task is running
	remote_bandw : int
		bandwidth in MB/s when accessing a file remotely from
		centralized storage
	cluster_net_bandw : int
		bandwidth in MB/s when transfering data between nodes
		of the cluster
	debug : bool
		should we print debug informations during the
		simulation?
	replication_period : int
		time in seconds between redoing the replication
		decisions
	ext_job_ocupation_rate : Float
		ocupation rate of the cluster by jobs that are external
		to our framework (here we call them "external jobs")
	ext_job_duration : int
		duration (in seconds) of the external jobs executed in
		the cluster
	cache_replacement_strategy : str
		the cache replacement strategy to be used.
		"score" removes the files with the lowest scores
		(enough to open the required space in the cache)
	metrics_reset_period : int
		time (in seconds) between consecutive resets of the
		metrics involved in the file score
	keep_remote_files : bool
		when a task run in a processing node accessing a file
		from the centralized storage, should a copy of this
		file be kept in that processing node after the task's
		execution?
	oracle : bool
		if replication and cache replacement policies should
		use scores based on the real reuse of the files (which
		is not usually known by the system, thus an oracle)
	task_scheduling_strategy : str
		the scheduler used for tasks. Options: "FIFO", "Greedy"

	Methods
	-------
	print()
		Make a string with all current parameters.
	read_from_file(filename)
		Redefines some (or all) of the parameters by reading
		them from an input text file. The file must have one
		parameter per line in the "parameter = value" format.
	"""
	# default values for the parameters
	def_p = 4
	def_stor_capacity = 6*1024  # 10GB
	def_f = 8
	def_file_size_avg = 2048  # 2GB
	def_file_size_std = 1024  # 1GB
	def_file_reuse_avg = 1
	def_replica_quota = 0.2
	def_replica_strategy = "objects"
	def_local_bandw = 800
	def_remote_bandw = 100
	def_cluster_net_bandw = 128
	def_debug = True
	def_replication_period = 30
	def_ext_job_ocupation_rate = 0.0
	def_ext_job_duration = 10
	def_cache_replacement_strategy = "score"
	def_metrics_reset_period = 10000
	def_keep_remote_files = True
	def_oracle = False
	def_task_scheduling_strategy = "Greedy"

	def __init__(self, p = def_p,
			stor_capacity = def_stor_capacity,
			f = def_f,
			file_size_avg=def_file_size_avg,
			file_size_std=def_file_size_std,
			file_reuse_avg=def_file_reuse_avg,
			replica_quota=def_replica_quota,
			replica_strategy = def_replica_strategy,
			local_bandw=def_local_bandw,
			remote_bandw=def_remote_bandw,
			cluster_net_bandw=def_cluster_net_bandw,
			debug = def_debug,
			replication_period=def_replication_period,
			ext_job_ocupation_rate=def_ext_job_ocupation_rate,
			ext_job_duration=def_ext_job_duration,
			cache_replacement_strategy=def_cache_replacement_strategy,
			metrics_reset_period = def_metrics_reset_period,
			keep_remote_files = def_keep_remote_files,
			oracle = def_oracle,
			task_scheduling_strategy = def_task_scheduling_strategy):
		"""
		Parameters
		----------
		p : int
			number of processing nodes
		stor_capacity : int
			in MB, storage capacity of the processing nodes
			(the amount available for storing files)
		f : int
			number of files.
		file_size_avg : int
			average file size in MB. We assume a normal
			distribution for the file size, defined by its average
			and standard deviation.
		file_size_std : int
			standard deviation for file size in MB.
			We assume a normal
			distribution for the file size, defined by its average
			and standard deviation.
		file_reuse_avg : int
			average number of times each file is going to be
			re-used (if it is zero, then it is going to be used
			once). We assume an exponential distribution for the
			file reuse factor.
		replica_quota : Float
			part (between 0 and 1) of the total cache capacity
			being used to store replicas in the nodes strategy,
			fraction of the number of files to be replicated in
			the objects replication strategy
		replica_strategy : str
			the replication strategy. "nodes" sets aside
			replica_quota of the nodes to only store replicas.
			"objects" replicates replica_quora*100 % of the files
		local_bandw : int
			bandwidth in MB/s when accessing a file that is stored
			in the node where the task is running
		remote_bandw : int
			bandwidth in MB/s when accessing a file remotely from
			centralized storage
		cluster_net_bandw : int
			bandwidth in MB/s when transfering data between nodes
			of the cluster
		debug : bool
			should we print debug informations during the
			simulation?
		replication_period : int
			time in seconds between redoing the replication
			decisions
		ext_job_ocupation_rate : Float
			ocupation rate of the cluster by jobs that are external
			to our framework (here we call them "external jobs")
		ext_job_duration : int
			duration (in seconds) of the external jobs executed in
			the cluster
		cache_replacement_strategy : str
			the cache replacement strategy to be used.
			"score" removes the files with the lowest scores
			(enough to open the required space in the cache)
		metrics_reset_period : int
			time (in seconds) between consecutive resets of the
			metrics involved in the file score
		keep_remote_files : bool
			when a task run in a processing node accessing a file
			from the centralized storage, should a copy of this
			file be kept in that processing node after the task's
			execution?
		oracle : bool
			if replication and cache replacement policies should
			use scores based on the real reuse of the files (which
			is not usually known by the system, thus an oracle)
		task_scheduling_strategy : str
			the scheduler used for tasks. Options: "FIFO", "Greedy"
		"""
		self.p = p
		self.stor_capacity = stor_capacity
		self.f = f
		self.file_size_avg = file_size_avg
		self.file_size_std = file_size_std
		self.file_reuse_avg = file_reuse_avg
		self.replica_quota = replica_quota
		self.replica_strategy = replica_strategy
		self.local_bandw = local_bandw
		self.remote_bandw = remote_bandw
		self.cluster_net_bandw = cluster_net_bandw
		self.debug = debug
		self.replication_period = replication_period
		self.ext_job_ocupation_rate = ext_job_ocupation_rate
		self.ext_job_duration = ext_job_duration
		self.cache_replacement_strategy = cache_replacement_strategy
		self.metrics_reset_period = metrics_reset_period
		self.keep_remote_files = keep_remote_files
		self.oracle = oracle
		self.task_scheduling_strategy = task_scheduling_strategy

	def print_str(self):
		"""
		Make a string with all current parameters.

		Returns
		-------
		str
			a string to be printed with all parameters
		"""
		ret = "Simulation parameters:\n"
		ret += "p = " + str(self.p) + "\n"
		ret += "stor_capacity = " + str(self.stor_capacity) + "\n"
		ret += "f = " + str(self.f) + "\n"
		ret += "file_size_avg = " + str(self.file_size_avg) + " MB\n"
		ret += "file_size_std = " + str(self.file_size_std) + " MB\n"
		ret += "file_reuse_avg = " + str(self.file_reuse_avg) + " times\n"
		ret += "replica_quota = " + str(self.replica_quota) + "\n"
		ret += "replica_strategy = " + str(self.replica_strategy) + "\n"
		ret += "local_bandw = " + str(self.local_bandw) + " MB/s\n"
		ret += "remote_bandw = " + str(self.remote_bandw) + " MB/s\n"
		ret += "cluster_net_bandw = " + str(self.cluster_net_bandw) + " MB/s\n"
		ret += "debug = " + str(self.debug) + "\n"
		ret += "replication_period = " + str(self.replication_period) + " s\n"
		ret += ("ext_job_ocupation_rate = " +
			str(self.ext_job_ocupation_rate) + "\n")
		ret += "ext_job_duration = " + str(self.ext_job_duration) + "s\n"
		ret += ("cache_replacement_strategy = " +
			self.cache_replacement_strategy + "\n")
		ret += "metrics_reset_period = " + str(self.metrics_reset_period) + "\n"
		ret += "keep_remote_files = " + str(self.keep_remote_files) + "\n"
		ret += "oracle = " + str(self.oracle) + "\n"
		ret += "task_scheduling_strategy = " + self.task_scheduling_strategy
		return ret

	def read_from_file(self, filename):
		"""
		Redefines some (or all) of the parameters by reading
		them from an input text file. The file must have one
		parameter per line in the "parameter = value" format.

		Parameters
		----------
		filename : str
			The name of the file to be read.
		"""
		arq = open(filename, "r")
		for line in arq:
			parsed = line.split('\n')[0].split(' ')
			parsed_str = ""
			for elem in parsed:
				for e in elem.split('\t'):
					parsed_str += e
			if parsed_str[0] == "#":
				continue  # this line is a comment
			parsed = parsed_str.split('#')[0].split('=')
			if(len(parsed) != 2):
				print("I don't understand this line!")
				print(line)
				continue
			if parsed[0] == "p":
				self.p = int(parsed[1])
			elif parsed[0] == "stor_capacity":
				self.stor_capacity = int(parsed[1])
			elif parsed[0] == "f":
				self.f = int(parsed[1])
			elif parsed[0] == "file_size_avg":
				self.file_size_avg = int(parsed[1])
			elif parsed[0] == "file_size_std":
				self.file_size_std = int(parsed[1])
			elif parsed[0] == "file_reuse_avg":
				self.file_reuse_avg = int(parsed[1])
			elif parsed[0] == "replica_quota":
				self.replica_quota = float(parsed[1])
			elif parsed[0] == "replica_strategy":
				self.replica_strategy = parsed[1]
			elif parsed[0] == "local_bandw":
				self.local_bandw = int(parsed[1])
			elif parsed[0] == "remote_bandw":
				self.remote_bandw = int(parsed[1])
			elif parsed[0] == "cluster_net_bandw":
				self.cluster_net_bandw = int(parsed[1])
			elif parsed[0] == "debug":
				if(parsed[1].upper() == "TRUE"):
					self.debug = True
				elif(parsed[1].upper() == "FALSE"):
					self.debug = False
				else:
					print("I dont understand this line!")
					print(line)
					continue
			elif parsed[0] == "replication_period":
				self.replication_period = int(parsed[1])
			elif parsed[0] == "ext_job_ocupation_rate":
				self.ext_job_ocupation_rate = float(parsed[1])
			elif parsed[0] == "ext_job_duration":
				self.ext_job_duration = int(parsed[1])
			elif parsed[0] == "cache_replacement_strategy":
				self.cache_replacement_strategy = parsed[1]
			elif parsed[0] == "metrics_reset_period":
				self.metrics_reset_period = int(parsed[1])
			elif parsed[0] == "keep_remote_files":
				if(parsed[1].upper() == "TRUE"):
					self.keep_remote_files = True
				elif(parsed[1].upper() == "FALSE"):
					self.keep_remote_files = False
				else:
					print("I dont understand this line!")
					print(line)
					continue
			elif parsed[0] == "oracle":
				if(parsed[1].upper() == "TRUE"):
					self.oracle = True
				elif(parsed[1].upper() == "FALSE"):
					self.oracle = False
				else:
					print("I dont understand this line!")
					print(line)
					continue
			elif parsed[0] == "task_scheduling_strategy":
				self.task_scheduling_strategy = parsed[1]
			else:
				print("I dont understand this line!")
				print(line)
				continue
		arq.close()
