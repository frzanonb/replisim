import random


class Task:
	"""
	Contains information about a task that is queued or running.

	...

	Attributes
	----------
	file : File
		file this task will access
	duration : Float
		for how long this task is going to run (updated once we
		know where it is scheduled)
	local : bool
		does the task has local access to the file? (updated
		once we know where it will run)
	param : SimulationParameters
		a reference to the simulation parameters object

	Methods
	-------
	update_duration(local)
		Called once the task is schedule to say if it will have
		local access to its file or not (and update is duration
		accordingly)
	"""
	def __init__(self, file_info, param):
		"""
		Parameters
		----------
		file_info : File
			file this task will access
		param : SimulationParameters
			a reference to the simulation parameters object
		"""
		self.file = file_info
		self.duration = 0.0
		self.local = False
		self.param = param

	def update_duration(self, local):
		"""
		Called once the task is schedule to say if it will have
		local access to its file or not (and update is duration
		accordingly)

		Parameters
		----------
		local : bool
			does the task has local access to the file?
		"""
		self.local = local
		if local:
			self.duration = (float(self.file.size) /
					float(self.param.local_bandw))
		else:
			self.duration = (float(self.file.size) /
					float(self.param.remote_bandw))


class TaskManager:
	"""
	Generic class for a task scheduler. It is NOT to be used
	directly, it is the parent class for the different scheduling
	strategies.

	...

	Attributes
	----------
	param : SimulationParameters
		a reference to the simulation parameters object
	tasks : List
		a list of Task objects with tasks to run (the queue)

	Methods
	-------
	print_queue()
		returns a string to be printed with information about
		all queued tasks.
	next_task()
		returns the next task to be scheduled and removes it
		from the queue.
	"""
	def __init__(self, param, filestore):
		"""
		Parameters
		----------
		param : SimulationParameters
			a reference to the simulation parameters.
		filestore : FileStore
			a reference to the FileStore object which contains the
			information about all files.
		"""
		self.param = param
		self.tasks = []
		for i in range(param.f):
			for r in range(filestore.files[i].reuse_factor):
				self.tasks.append(Task(filestore.files[i],
							param))
		# shuffle this list to make the queue
		random.shuffle(self.tasks)

	def print_queue(self):
		"""
		returns a string to be printed with information about
		all queued tasks.

		Returns
		-------
		str
			a string with information about the queue
		"""
		ret = str(len(self.tasks)) + " queued tasks: "
		for i in range(len(self.tasks)):
			ret += str(self.tasks[i].file.id)
			if i != (len(self.tasks)-1):
				ret += ", "
		return ret

	def next_task(self):
		"""
		returns the next task to be scheduled and removes it
		from the queue.

		Raises
		------
		NotImplementedError
			if called with the parent class, all children
			MUST implement this.
		"""
		raise NotImplementedError


class FIFOTaskManager(TaskManager):
	"""
	Implements the FIFO task scheduler, which schedules tasks in
	their arrival order (the order they appear in the queue).
	"""
	def next_task(self):
		ret = self.tasks[0]
		self.tasks = self.tasks[1:]
		return ret


class GreedyTaskManager(TaskManager):
	"""
	Implements the Greedy task scheduler, which tries to schedule
	tasks that access files that are stored by one of the currently
	available nodes (so they will have local access). If no such
	task exists, take them in FIFO order.

	...

	Attributes
	----------
	cluster : Cluster
		a reference to the Cluster object so we can verify the
		list of available nodes
	"""
	def __init__(self, param, filestore, cluster):
		"""
		Parameters
		----------
		cluster : Cluster
			a reference to the Cluster object so we can verify the
			list of available nodes
		"""
		TaskManager.__init__(self, param, filestore)
		self.cluster = cluster

	def next_task(self):
		found = False
		for task in self.tasks:
			if self.cluster.is_local_access_possible(task.file):
				found = True
				break
		if not found:
			# there is no task with local access that is
			# possible right now, so we just take the first
			# from the queue (FIFO)
			task = self.tasks[0]
		self.tasks.remove(task)
		return task
