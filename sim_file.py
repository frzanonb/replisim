import numpy as np
import heapq


class File:
	"""
	Information about each file in the simulation.

	...

	Attributes
	----------
	id : int
		identifier of this file
	size : int
		size of the file in MB
	reuse_factor : int
		number of times the file is going to be used (number
		of tasks that will access it)
	dest : List
		list of Node objects for all the nodes that store this
		file
	accesses : int
		number of accesses (each task that executes generates
		one access)
	score : Float
		a score to represent the importance of this file (used
		to make decisions about replication and cache
		replacement).
	raw_score : Float
		the score is normalized by the highest value
		across all files, we keep the originally
		calculated (before normalizing) value here.
	param : SimulationParameters
		a reference to the simulation parameters object

	Methods
	-------
	written(node)
		Called when a copy of this file has been written to a
		node
	removed(node)
		Called when a copy of this file has been removed from a
		node.
	calculate_raw_score(new_replicas=0)
		Called to update the score. The score depends on the
		number of recent accesses and the number of existing
		replicas. Only the raw score will be updated, not the
		regular normalized score. If the new_replicas parameter
		is provided, we calculate the score considering some
		additional replicas. That is used by replication
		strategies to see how new replicas affect the scores.
	normalize_score(max_score)
		Called to calculate score using raw_score, normalizing
		by the provided max score.
	reset_score_metrics()
		Reset the metrics that affect the calculation of score.
		(for now just the number of accesses).
	print()
		Return a string to be printed with information about
		this file.
	"""
	def __init__(self, ident, size, reuse, param):
		"""
		Parameters
		----------
		ident : int
			identifier of this file
		size : int
			size of the file in MB
		reuse : int
			number of times the file is going to be used (number
			of tasks that will access it)
		param : SimulationParameters
			a reference to the simulation parameters object
		"""
		self.id = ident
		self.size = size
		self.reuse_factor = reuse
		self.dest = []
		self.accesses = 0
		self.score = 0.0
		self.raw_score = 0.0
		self.param = param

	def written(self, node):
		"""
		Called when a copy of this file has been written to a
		node

		Parameters
		----------
		node : Node
			the node that now stores this file
		"""
		if not (node in self.dest):
			self.dest.append(node)
		else:
			print("Warning! Two copies of the same file in the same node?")

	def removed(self, node):
		"""
		Called when a copy of this file has been removed from a
		node.

		Parameters
		----------
		node : Node
			the node that no longer stores this file
		"""
		if node in self.dest:
			self.dest.remove(node)

	def calculate_raw_score(self, new_replicas=0):
		"""
		Called to update the score. The score depends on the
		number of recent accesses and the number of existing
		replicas. Only the raw score will be updated, not the
		regular normalized score. If the new_replicas parameter
		is provided, we calculate the score considering some
		additional replicas. That is used by replication
		strategies to see how new replicas affect the scores.

		Parameters
		----------
		new_replicas : int (optional)
			If provided, how many new replicas of this
			file we should consider when calculating the
			score.
		"""
		if (len(self.dest)+new_replicas) <= 0:
			self.raw_score = 0.0
		else:
			if self.param.oracle:
				self.raw_score = (float(self.reuse_factor-self.accesses) /
						float(len(self.dest)+new_replicas))
			else:
				self.raw_score = (float(self.accesses) /
						float(len(self.dest)+new_replicas))

	def normalize_score(self, max_score):
		"""
		Called to calculate score using raw_score, normalizing
		by the provided max score.

		Parameters
		----------
		max_score : Float
			the maximum score that will be used to
			calculate our normalized score.
		"""
		if max_score > 0:
			self.score = self.raw_score / max_score
		else:
			# it will only happen at the beginning, later
			# we should have a max score higher than 0
			self.score = 0.0

	def reset_score_metrics(self):
		"""
		Reset the metrics that affect the calculation of score.
		(for now just the number of accesses).
		"""
		self.accesses = 0

	def print(self):
		"""
		Return a string to be printed with information about
		this file.

		Returns
		-------
		str
			a string with information about this file
		"""
		ret = ("File " + str(self.id) + " of size " + str(self.size) +
			" and reuse factor " + str(self.reuse_factor) +
			" has been accessed " + str(self.accesses) +
			" times. It is stored in " + str(len(self.dest)) +
			" nodes and has current score of " + str(self.score) +
			". The nodes are: ")
		for i in self.dest:
			ret += str(i.id)
			if i != self.dest[len(self.dest)-1]:
				ret += ", "
		return ret

	def __lt__(self, other):
		"""
		We implement the < comparison used when adding files to
		a heap.

		Parameters
		----------
		other : File
			other object to compare to this one

		Returns
		-------
		bool
			is this object smaller than the other one?
		"""
		if self.score == other.score:
			return self.id < other.id
		else:
			return self.score < other.score


class FileStore:
	"""
	The collection of all files in the simulation.

	...

	Attributes
	----------
	files : List
		a list of File objects containing all files in the
		simulation, indexed by their identifiers.

	Methods
	-------
	distribute_files(node_info, nodes_nb)
		Called at the beginning of the simulation to store the
		whole set of files in a set of nodes randomly but
		trying to keep the load balanced among the nodes.
		IMPORTANT: we don't check if there is space for new
		files, that means some files may be erased when we ask
		to write new ones.
	reset_score_metrics()
		Called periodically to reset the metrics involved in
		the calculation of file scores. It calls the
		reset_score_metrics method for all files stored in this
		node.
	"""
	def __init__(self, param):
		"""
		Parameters
		----------
		param : SimulationParameters
			a reference to the simulation parameters object
		"""
		self.files = []
		sizes = np.random.normal(param.file_size_avg,
					param.file_size_std,
					param.f)
		sizes_sum = 0
		for s in range(len(sizes)):
			# make sure we don't have files with size 0 or
			# less (it is unlikely but it could happen), or
			# files larger than the storage capacity of the
			# processing nodes
			while ((sizes[s] <= 0) or (sizes[s] > param.stor_capacity)):
				sizes[s] = np.random.normal(param.file_size_avg,
								param.file_size_std)
			sizes_sum += int(sizes[s])
		print("The generated files make for a total of " +
			str(sizes_sum) +
			" MB of data")
		reuse = np.random.exponential(param.file_reuse_avg, param.f)
		for i in range(param.f):
			# reuse factor + 1 so it will be the number of
			# times the file will be used (not only
			# re-used)
			self.files.append(File(i, int(sizes[i]), int(reuse[i])+1, param))

	def distribute_files(self, node_info, nodes_nb):
		"""
		Called at the beginning of the simulation to store the
		whole set of files in a set of nodes randomly but
		trying to keep the load balanced among the nodes.
		IMPORTANT: we don't check if there is space for new
		files, that means some files may be erased when we ask
		to write new ones.

		Parameters
		----------
		node_info : List
			list of all Node objects for all nodes
		nodes_nb : int
			the number of nodes to be used for storing the
			files (if smaller then the total number of
			nodes we'll take the first nodes from the list)
		"""
		# make a priority queue (a heap) to store pairs (size
		# of stored data, node) and always select the one with
		# the smallest amount of written data to write the next
		# file
		cache_nodes = []
		for i in range(nodes_nb):
			cache_nodes.append((node_info[i].volume, node_info[i]))
		heapq.heapify(cache_nodes)
		for i in range(len(self.files)):
			# get from the heap the node with less data
			dest = heapq.heappop(cache_nodes)[1]
			# write the file there
			dest.write(self.files[i])
			# put the node back to the heap with updated
			# amount of written data
			heapq.heappush(cache_nodes, (dest.volume, dest))

	def reset_score_metrics(self):
		"""
		Called periodically to reset the metrics involved in
		the calculation of file scores. It calls the
		reset_score_metrics method for all files stored in this
		node.
		"""
		for file_info in self.files:
			file_info.reset_score_metrics()

	def calculate_file_scores(self):
		"""
		Calculates the scores of all files in the system,
		normalizes them, and returns the maximum score.

		Returns
		-------
		max_score : Float
			The maximum score among the files stored in
			this node.
		"""
		# calculate the score for each file, and find out the
		# maximum value
		max_score = 0.0
		for f in range(len(self.files)):
			self.files[f].calculate_raw_score()
			if self.files[f].raw_score > max_score:
				max_score = self.files[f].raw_score
		# now normalize all scores
		if max_score > 0.0:
			for f in range(len(self.files)):
				self.files[f].normalize_score(max_score)
				assert (self.files[f].score <= 1.0) and (self.files[f].score >= 0.0)
		return max_score
