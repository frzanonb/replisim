import numpy as np
from sim_input import SimulationParameters
from sim_output import SimulationOutput, transfer_list_mkstring
from node import Node


class Cluster:
	"""
	The collection of nodes where tasks are executed.

	...

	Attributes
	----------
	p : int
		number of processing nodes in this cluster
	nodes : List
		a list of Node objects, one per node of the cluster,
		indexed by node identifier (the node identifier is its
		position in this list).
	available_nodes : List
		a list of Node objects that are available to run tasks
		(not currently being used).
	external_job_nodes : List
		a list of Node objects, the nodes being used by
		external jobs of the cluster.
	param : SimulationOutput
		a reference to the simulation parameters object  end of
		the simulation
	output : SimulationOutput
		a reference to the output object that keeps metrics to
		be reported at the end of the simulation
	events : EventList
		a reference to the events heap so we can push new events

	Methods
	-------
	free(node)
		Called when a node is now available (upon finishing to
		run something in it).
	random_pick()
		Randomly remove a node from the available nodes list
		and returns it.
	locality_node_or_random(file)
		Remove and return an available node containing the file
		given as parameter. If we cannot find it, return one at
		random. Also return a boolean to say if the file
		contains the file or not.
	is_local_access_possible(file)
		For a given file, answer if there is an available node
		(right now) that contains that file. It is used for
		task scheduling.
	initial_external_jobs()
		Makes the initial setup of external jobs running on the
		processing nodes (before starting the simulation).
	possibly_schedule_external_job(node)
		Called for a node recently freed. This node has a
		probability of being picked to run an external job. If
		that is the case, we schedule the external job.
	"""
	def __init__(self, param, output, events, replace):
		"""
		Parameters
		----------
		param : SimulationOutput
			a reference to the simulation parameters object  end of
			the simulation
		output : SimulationOutput
			a reference to the output object that keeps metrics to
			be reported at the end of the simulation
		events : EventList
			a reference to the events heap so we can push new events
		replace : CacheReplacementStrategy
			a reference to the cache replacemnt policy in use
		"""
		self.p = param.p
		self.param = param
		self.events = events
		self.output = output
		# create the nodes
		self.available_nodes = []
		self.nodes = []
		for i in range(param.p):
			# make a new object to each node with no files stored in it
			self.nodes.append(Node(i, output, param, replace))
			# they are all available at first
			self.available_nodes.append(self.nodes[i])
		self.external_job_nodes = []

	def free(self, node):
		"""
		Called when a node is now available (upon finishing to
		run something in it).

		Parameters
		----------
		node : Node
			the node that is now free

		Returns
		-------
		str
			A debug string to be printed about this
			operation.
		"""
		previous_external = len(self.external_job_nodes)
		assert not (node in self.available_nodes)
		self.available_nodes.append(node)
		if node in self.external_job_nodes:
			# this will only be true if this node is being
			# free from usage by an external job, not by
			# our own tasks
			self.external_job_nodes.remove(node)
			node.external = False
		# every time a node is made available it could be used
		# for external jobs of the cluster
		ret = self.possibly_schedule_external_job(node)
		if len(self.external_job_nodes) != previous_external:
			# we avoid to update the external usage metrics
			# from the output object every time we finish
			# or start an external job, instead we only do
			# so when the number of nodes changes
			self.output.update_external_jobs(len(self.external_job_nodes))
		return ret

	def random_pick(self):
		"""
		Randomly remove a node from the available nodes list
		and returns it.

		Returns
		-------
		Node
			the node that is no longer in the available
			nodes list
		"""
		# choose one node randomly
		node = self.available_nodes[np.random.randint(0, len(self.available_nodes))]
		self.available_nodes.remove(node)
		return node

	def locality_node_or_random(self, file_info):
		"""
		Remove and return an available node containing the file
		given as parameter. If we cannot find it, return one at
		random. Also return a boolean to say if the file
		contains the file or not.

		Parameters
		----------
		file_info : File
			a file

		Returns
		-------
		Node
			the node that is no longer in the available
			nodes list
		bool
			does the returned node contain the file given
			as a parameter?
		"""
		found = False
		for node in self.available_nodes:
			if file_info in node.myfiles:
				found = True
				break
		if found:
			# remove the selected node from the list of
			# available ones
			self.available_nodes.remove(node)
		else:
			node = self.random_pick()
		return (node, found)

	def is_local_access_possible(self, file_info):
		"""
		For a given file, answer if there is an available node
		(right now) that contains that file. It is used for
		task scheduling.

		Parameters
		----------
		file_info : File
			a file

		Returns
		-------
		bool
			is there an available node that stores this
			file?
		"""
		found = False
		for node in self.available_nodes:
			if file_info in node.myfiles:
				found = True
				break
		return found

	def initial_external_jobs(self):
		"""
		Makes the initial setup of external jobs running on the
		processing nodes (before starting the simulation).

		Returns
		-------
		str
			A debug string to be printed about this
			operation.
		"""
		# go through all nodes, each of them has a probability
		# of being taken
		ret = ""
		for node in self.nodes:
			if ret != "":
				ret += "\n"
			ret += self.possibly_schedule_external_job(node)
		return ret

	def possibly_schedule_external_job(self, node):
		"""
		Called for a node recently freed. This node has a
		probability of being picked to run an external job. If
		that is the case, we schedule the external job.

		Returns
		-------
		str
			A debug string to be printed about this
			operation.
		"""
		ret = ""
		if len(node.transfers) == 0:
			# nodes that are transferring files for our
			# framework are not really free for external
			# usage
			luck = np.random.randint(1, 101)
			if luck <= (self.param.ext_job_ocupation_rate * 100):
				# this node is going to run an external
				# job now!
				self.external_job_nodes.append(node)
				self.available_nodes.remove(node)
				node.external = True
				self.events.push(self.param.ext_job_duration,
							ExternalJob(node))
				# we always print scheduled external
				# jobs for reproducibility
				ret += (str(self.events.clock) +
					" Scheduled an external job in node " +
					str(node.id))
		else:
			if self.param.debug:
				ret += ("node " + str(node.id) +
					" cannot run external jobs because it is involved in these transfers: ")
				ret += transfer_list_mkstring(node.transfers)
		return ret
