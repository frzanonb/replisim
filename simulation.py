"""
Script that runs a simulation

It may receive as an argument the name of a text file containing the 
simulation parameters. 
If no parameters file is provided, the default values (defined in 
sim_input.py will be used. Refer to example.param for an example of 
parameters file.

At the end, the script will print information about what happened.
"""

import sys
import heapq
from replica import ObjectReplicaStrategy, NodeReplicaStrategy
from event import EventList, EventInfo, TaskRun, LocalTransfer
from cluster import Cluster
from sim_input import SimulationParameters
from sim_output import SimulationOutput, print_node_list, print_event_list, print_transfer_list
from sim_file import FileStore
from cache_replacement import ScoreCacheReplacement
from task import GreedyTaskManager

param = SimulationParameters() 		# parameters of the simulation 
# if we received a filename as a parameter, read parameters from it
if(len(sys.argv) > 1): 
	param.read_from_file(sys.argv[1])
print(param.print_str())
# our simulation will be guided by events. 
# each event has a time when it will happen, and we use a priority 
# queue (a heap) to keep track of the events (so we can easily see what
# is the next one to take place)
events = EventList()
output = SimulationOutput(param, events) 	# output statistics
#######################################################################
# initial setup
#######################################################################
# generate the files (managed by a FileStore) 
filestore = FileStore(param)
if param.debug:
	print("--------------------------------")
	print("created the "+str(param.f)+" files")
# the cache replacement strategy
if param.cache_replacement_strategy == "score":
	replace = ScoreCacheReplacement(param, filestore)
else:
	print("PANIC! I dont know your cache replacement strategy " +
		param.cache_replacement_strategy)
	exit()
# the replication strategy
if param.replica_strategy == "nodes": 
	replicate = NodeReplicaStrategy(output, filestore)
elif param.replica_strategy == "objects":
	replicate = ObjectReplicaStrategy(output, filestore, param)
else:
	print("PANIC! I dont know your replica strategy " +
		param.replica_strategy)
	exit()
# create the nodes and the list of available nodes
cluster = Cluster(param, output, events, replace)
if param.debug:
	print("--------------------------------")
	print("created the "+str(param.p)+" nodes")
# cache all files by randomly asigning them to nodes.
# nodes_nb is the number of nodes that will receive new files at the 
# beginning (as opposed to the nodes that receive replicas of other 
# nodes). That is usually the total number of nodes, except when using
# the "nodes" replica strategy
nodes_nb = param.p	
if replicate.name == "nodes":
	nodes_nb = int((1 - param.replica_quota) * param.p)
filestore.distribute_files(cluster.nodes, nodes_nb)
if param.debug:
	print("--------------------------------")
	print("cached all files:")
	for i in range(param.f):
		print(filestore.files[i].print())
	for i in range(param.p):
		print(cluster.nodes[i].print())
	print("--------------------------------")
# make replicas.
# IMPORTANT: here we are making the replicas during the setup, so we
# consider they already exist at the beginning and we don't count the
# time to create them. 
replicate.initial_replicas(filestore.files, cluster.nodes, nodes_nb)	
# we always print the initial state of the system for reproducibility
print("--------------------------------")
print("made the initial replicas")
for i in range(param.f):
	print(filestore.files[i].print())
for i in range(param.p):
	print(cluster.nodes[i].print())
# create a list of all tasks. Each file will create one or multiple
# tasks (depending on its reuse factor)
if param.task_scheduling_strategy == "FIFO":
	scheduler = FIFOTaskManager(param, filestore)
elif param.task_scheduling_strategy == "Greedy":
	scheduler = GreedyTaskManager(param, filestore, cluster)
else:
	print("PANIC! I dont know this task scheduler " +
		param.task_scheduling_strategy)
	exit()
# we always print the list of tasks (after shuffling) for
# reproducibility 
print("--------------------------------")
print(scheduler.print_queue())
print("Ill start the simulation now")
# we have a periodic event of rethinking the replication decisions
events.push(param.replication_period, EventInfo(1))
# we have another periodic event to reset the metrics involved in the
# score
events.push(param.metrics_reset_period, EventInfo(4))
# the interconnection between centralized storage and processing nodes
# has a current bandwidth being used by tasks accessing files from the
# centralized storage 
current_bandwidth = 0
# the same but for the connection between processing nodes (we see it
# as a whole)
current_inside_bandwidth = 0
# at the beginning some nodes may already be used by external jobs
external_log = cluster.initial_external_jobs()
if(external_log != ""):
	print(external_log)
#######################################################################
# simulation
#######################################################################
# we keep the number of tasks that are running so we know when we can
# end the simulation
running_tasks = 0 	
# we have an identifier to each time we review our replication
# decisions because we'll use it to stamp the transfers it generates.
# For instance, if in the review 1 we generate a transfer event that
# only finishes after review 2, when it is done we'll know we need to
# ignore it.
replicate_review = 0	
# while we have tasks to run or we are still running stuff
while (len(scheduler.tasks) > 0) or (running_tasks > 0): 
	# schedule tasks if/while we can
	scheduled = False
	previous_bandwidth = current_bandwidth
	while (len(scheduler.tasks) > 0) and (len(cluster.available_nodes) > 0): 
		# we have both tasks to run and nodes where to run them
		scheduled = True
		# take the next task to run 
		task = scheduler.next_task()
		# do we have an available node that has data this task
		# needs?
		node, found = cluster.locality_node_or_random(task.file)
		# contabilize the access to this file and calculate the
		# execution time of the task
		task.file.accesses += 1  
		task.update_duration(found)
		# if the access is remote, we will have a transfer of
		# data in the interconnection between centralized
		# storage and the cluster
		if not found:	
			current_bandwidth += param.remote_bandw
		# create an event for when the task is done and put it
		# in our heap
		events.push(task.duration, TaskRun(task, node))
		running_tasks += 1
	if param.debug and scheduled:
		print("--------------------------------")
		print("scheduled some tasks (" +
			str(len(scheduler.tasks)) + 
			" remain in the queue)!")
		print_node_list(cluster.available_nodes)
		print("current bandwidth is "+str(current_bandwidth))
		print_event_list(events.events)
	if previous_bandwidth != current_bandwidth:  
		# we update the state of the connection to the
		# centralized storage if that has changed after new
		# tasks were scheduled
		output.update_outside_transfer(current_bandwidth)
	# there is nothing we can do at this time so we fast forward to
	# the next event
	event = events.pop()
	if param.debug: 
		print("--------------------------------")
		print("moved to time "+str(events.clock) +
			" and popped an event: " + 
			event.print())
	# what happened?
	if event.type == 0:   # a task finished its execution
		running_tasks -= 1
		# update output metrics
		output.executed(event.task.local)
		# we release the network we were using if the access
		# was remote
		if not event.task.local:
			current_bandwidth -= param.remote_bandw
			output.update_outside_transfer(current_bandwidth)
		# if the access was remote, is the file now in the cache
		# of that node?
		if (param.keep_remote_files) and (not event.task.local) and (not (event.task.file in event.node.myfiles)):  
			# since the execution of this task, the file
			# could have been copied into this node due to
			# replication decisions
			event.node.write(event.task.file)
		# this node is free now
		external_log = cluster.free(event.node)
	elif event.type == 1: 
		# it is time to review our decisions on replications
		replicate_review += 1
		# review the decisions and get the list of files that
		# need to be copied between nodes
		transfers, review_log = replicate.review(filestore.files, 
							cluster.nodes)
		# create events for all transfers
		previous_inside_bandwidth = current_inside_bandwidth
		for t in transfers:
			# we can only create transfers between nodes
			# that are not running external jobs
			assert not (t.source in cluster.external_job_nodes)  
			assert not (t.dest in cluster.external_job_nodes)
			duration = float(t.file.size) / float(param.cluster_net_bandw)
			events.push(duration, 
					LocalTransfer(t, 
						duration, 
						replicate_review))
			current_inside_bandwidth += param.cluster_net_bandw
			# the nodes need to have the information of
			# being involved in transfers
			t.source.transfer(t) 
			if (duration >= (0.5*param.replication_period)): 
				# this is just to help us choosing good
				# parameters
				print("WARNING! It is possible your replication_period is too short, as we have a transfer taking over half of it...")
		# this is a periodic event, so set up the next one
		events.push(param.replication_period, event)
		# keep track (for output metrics) of data transfers
		# between nodes
		if previous_inside_bandwidth != current_inside_bandwidth:
			output.update_inside_transfer(current_inside_bandwidth)
	elif event.type == 2: 
		# a file transfer between two nodes of the cluster is
		# done (for replication).
		# we check if we still want this replica
		if event.version == replicate_review: 
			if not (event.transfer.file in event.transfer.dest.myfiles): 
				# it could be the case if while this
				# transfer was happening a task
				# executed in the destination node to
				# access this same file with remote
				# access (after the remote access we
				# keep a copy of the file in the local
				# cache)
				event.transfer.dest.write(event.transfer.file) 	
		# the nodes need to know they are no longer involved in
		# this transfer
		event.transfer.source.end_transfer(event.transfer) 
		current_inside_bandwidth -= param.cluster_net_bandw
		output.update_inside_transfer(current_inside_bandwidth)
	elif event.type == 3: 
		# an external job has finished its execution
		# the node is now free to execute other jobs (of the
		# framework or external)
		external_log = cluster.free(event.node)
	elif event.type == 4: 
		# it is time to reset all metrics involved in the score
		filestore.reset_score_metrics()
		# this is a periodic event, so set up the next one
		events.push(param.metrics_reset_period, event)
	else:
		print("PANIC! I don't know this event??? " +
			event.print())
		continue	
	if param.debug: 
		if event.type == 1:
			print(review_log)
			print_transfer_list(transfers)
		elif (event.type == 0) or (event.type == 3):
			if(external_log != ""):
				print(external_log)
			print_node_list(cluster.available_nodes)
			print("current bandwidth is " +
				str(current_bandwidth))
		else:
			print_node_list(cluster.available_nodes)
			print("current bandwidth is " +
				str(current_bandwidth))
print("--------------------------------")
print("Total simulated time: "+str(events.clock))		
if events.clock > param.metrics_reset_period:
	print("Warning: it was enough to reset the score metrics")
print(output.report())
