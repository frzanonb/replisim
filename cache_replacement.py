import heapq


class CacheReplacementStrategy:
	"""
	Generic cache replacement class, NOT to be used directly but to
	be the parent of policy implementations.

	...

	Attributes
	----------
	name : str
		the name of the policy being used, provided by the
		initialization of the class implementing that policy.
	param : SimulationParameters
		a reference to the simulation parameters.

	Methods
	-------
	replace(node, size)
		frees the given size from the given node's local
		storage device following the policy. This method MUST
		be implemented by children.
	"""
	def __init__(self, name, param):
		"""
		Parameters
		----------
		name : str
			the name of the policy being used, provided by
			the initialization of the class implementing
			that policy.
		param : SimulationParameters
			a reference to the simulation parameters.
		"""
		self.name = name
		self.param = param

	def replace(self, node, size):
		"""
		Frees the given size from the given node's local
		storage device following the policy. This method MUST
		be implemented by children.

		Parameters
		----------
		node : Node
			the node with the full storage device
		size : int
			how much free space we need to create

		Raises
		------
		NotImplementedError
			if called with the parent class, all children
			MUST implement this.
		"""
		raise NotImplementedError


class ScoreCacheReplacement(CacheReplacementStrategy):
	"""
	Implements the cache replacement by score.

	...

	Attributes
	----------
	filestore : FileStore
		a reference to the FileStore object which contains the
		information about all files.
	"""
	def __init__(self, param, filestore):
		"""
		Parameters
		----------
		filestore : FileStore
			a reference to the FileStore object which
			contains the information about all files.
		"""
		CacheReplacementStrategy.__init__(self, "score", param)
		# we keep a reference to the filestore object so we
		# can call the function to calculate scores of all
		# files
		self.filestore = filestore

	def replace(self, node, size):
		"""
		Called when we want to write something but we can't
		because there is not enough space in the local cache,
		so we need to remove a file.
		"""
		assert (self.param.stor_capacity - node.volume) < size
		if self.param.debug:
			print("Some files will need to be removed from the local cache of node " +
				str(node.id))
		# calculate score for all my files because we'll remove
		# the lowest scores. For that we'll use a heap
		self.filestore.calculate_file_scores()
		file_scores = []
		for file_info in node.myfiles:
			assert (file_info.score >= 0.0) and (file_info.score <= 1.0)
			file_scores.append((file_info.score, file_info))
		heapq.heapify(file_scores)
		# erase as many files as necessary, always taking the
		# lowest score
		while (self.param.stor_capacity - node.volume) < size:
			# we limit the file sizes to what can fit in
			# cache, so we should always be able to make
			# room for any file
			assert len(file_scores) > 0
			pair = heapq.heappop(file_scores)
			file_info = pair[1]
			node.remove(file_info)
