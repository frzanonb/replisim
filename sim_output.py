import numpy as np


class SimulationOutput:
	"""
	Keeps statistics during the simulations and reports them at the
	end.

	Attributes
	----------
	write : int
		amount of data written to each node's local device in
		MB
	cache_usage : List
		proportion of the local cache of each node being used
		over the execution. One element per node, and for each
		node we'll have a list
		where each element is a pair (timestamp, occupancy
		rate)
	remove : List
		a list of int. Amount of data removed from each node's
		local device in MB.
	perf : List
		a list of Float. Telative performance of all executed
		tasks (relative to executing with local access to file)
	remoteperf : Float
		this is the relative performance when we have remote
		access. we keep it pre-calculated.
	net : List
		list of int. Bandwidth in the interconnection between
		centralized storage and the cluster over time. Each
		element is a pair (timestamp, bandwidth), and that
		bandwidth remains the same until the timestamp of the
		next entry
	param : SimulationParameters
		a reference of the simulation parameters object so we
		can check it
	events : EventList
		a reference to the events object so we can access the
		current clock to log things that happen
	external_usage : List
		a list of int. The number of nodes being used to run
		external jobs over time (a list of
		(timestamp, number of nodes))
	local_transfers : List
		a list of int. Bandwidth in the interconnection between
		processing nodes over the execution (a list of
		(timestmap, bandwidth)

	Methods
	-------
	written(node, size)
		Called when size MB are written to node.
	removed(node, size)
		Called when size MB were removed from the node's
		storage.
	update_inside_transfer(band)
		Called to register the demand on the connection between
		the processing nodes caused by the transfers between
		them (for replication).
	update_outside_transfer(band)
		Called to register the demand of the connection to the
		centralized storage has changed.
	new_capacity(node)
		Called to notify a node has changed its ocupation of
		its local cache. We don't update it in the written
		function because the Node.write operation may involve
		some file removals as well, it is cleaner to just
		update the capacity once.
	executed(local)
		Called when a task is executed to keep track of its
		performance.
	update_external_jobs(node_nb)
		Called when the number of nodes being used by external
		jobs changes.
	report()
		Makes a string to be printed containing all metrics
	report_list(this)
		Used by the report() method. Receives a list of numbers
		and makes a string with min, max, avg, median and the
		whole list.
	"""
	def __init__(self, param, events):
		"""
		Parameters
		----------
		param : SimulationParameters
			a reference of the simulation parameters object so we
			can check it
		events : EventList
			a reference to the events object so we can access the
			current clock to log things that happen
		"""
		self.write = []
		self.cache_usage = []
		self.remove = []
		for i in range(param.p):
			self.write.append(0)
			self.remove.append(0)
			self.cache_usage.append([])
		self.perf = []
		self.remoteperf = float(param.remote_bandw) / float(param.local_bandw)
		self.net = []
		self.param = param
		self.events = events
		self.external_usage = []
		self.local_transfers = []

	def written(self, node, size):
		"""
		Called when size MB are written to node.

		Parameters
		----------
		node : Node
			the node
		size : int
			amount of data in MB
		"""
		self.write[node] += size

	def removed(self, node, size):
		"""
		Called when size MB were removed from the node's
		storage.

		Parameters
		----------
		node : Node
			the node
		size : int
			amount of data in MB
		"""
		self.remove[node] += size

	def update_inside_transfer(self, band):
		"""
		Called to register the demand on the connection between
		the processing nodes caused by the transfers between
		them (for replication).

		Parameters
		----------
		band : int
			the bandwidth being used by transfers between
			processing nodes.
		"""
		self.local_transfers.append((self.events.clock, band))

	def update_outside_transfer(self, band):
		"""
		Called to register the demand of the connection to the
		centralized storage has changed.

		Parameters
		----------
		band : int
			the bandwidth being used by transfers between
			processing nodes and the centralized storage.
		"""
		self.net.append((self.events.clock, band))

	def new_capacity(self, node):
		"""
		Called to notify a node has changed its ocupation of
		its local cache. We don't update it in the written
		function because the Node.write operation may involve
		some file removals as well, it is cleaner to just
		update the capacity once.

		Parameters
		----------
		node : Node
			the node
		"""
		self.cache_usage[node.id].append((self.events.clock,
						float(node.volume) /
							float(self.param.stor_capacity)))

	def executed(self, local):
		"""
		Called when a task is executed to keep track of its
		performance.

		Parameters
		----------
		local : bool
			did the task execute with local access to its
			file?
		"""
		if local:
			# local access, best possible performance
			self.perf.append(1.0)
		else:
			# remote access
			self.perf.append(self.remoteperf)
			# IMPORTANT: here we measure performance in a
			# way that does not consider the size of the
			# files. Is that what we want?

	def update_external_jobs(self, node_nb):
		"""
		Called when the number of nodes being used by external
		jobs changes.

		Parameters
		----------
		node_nb : int
			the number of nodes being used by external jobs
		"""
		self.external_usage.append((self.events.clock, node_nb))

	def report(self):
		"""
		Makes a string to be printed containing all metrics

		Returns
		-------
		str
			a stirng containing all metrics
		"""
		ret = "amount of data written per node (MB)\n"
		ret += self.report_list(self.write)
		ret += "amount of data erased per node (MB)\n"
		ret += self.report_list(self.remove)
		ret += "performance of the tasks (relative to local access):\n"
		ret += self.report_list(self.perf)
		ret += "number of nodes used by external jobs\n"
		ret += str(self.external_usage) + "\n"
		ret += "usage of the local cache per node\n"
		for node in range(len(self.cache_usage)):
			ret += ("Node " + str(node) +
				": " + str(self.cache_usage[node]) + "\n")
		ret += "usage of the interconnection between centralized \
			storage and the cluster (MB/s) over time:\n"
		ret += str(self.net) + "\n"
		ret += "usage of the interconnection between processing \
			nodes (MB/s) over time:\n"
		ret += str(self.local_transfers) + "\n"
		return ret

	def report_list(self, this):
		"""
		Used by the report() method. Receives a list of numbers
		and makes a string with min, max, avg, median and the
		whole list.

		Parameters
		----------
		this : List
			a List of numbers.
		"""
		stri = ("min " + str(np.min(this)) +
			", max " + str(np.max(this)) +
			", avg " + str(np.mean(this)) +
			", median " + str(np.median(this)) + "\n")
		stri += str(this) + "\n"
		return stri


#######################################################################
def print_event_list(events):
	"""
	Prints a list of events and their informations.

	Parameters
	----------
	events : EventList
		the list of events
	"""
	ret = str(len(events)) + " events: "
	for i in range(len(events)):
		ret += "(" + str(events[i][0]) + ", " + events[i][1].print() + ")"
		if i < (len(events)-1):
			ret += ", "
	print(ret)


def print_node_list(nodes):
	"""
	Prints a list of nodes (to each node prints its id)

	Parameters
	----------
	nodes : List
		the list of Node objects for all nodes
	"""
	ret = str(len(nodes)) + " available_nodes: ["
	for n in nodes:
		ret += str(n.id)
		if n != nodes[len(nodes)-1]:
			ret += ", "
	ret += "]"
	print(ret)


def print_file_list(files, name):
	"""
	Prints a list of files (to each file prints its id)

	Parameters
	----------
	files : List
		the list of File objects for all files in the
		simulation
	name : str
		a name for this list
	"""
	ret = str(len(files)) + " " + name + ": ["
	for i in range(len(files)):
		ret += str(files[i].id)
		if i != (len(files)-1):
			ret += ", "
	ret += "]"
	print(ret)


def transfer_list_mkstring(transfers):
	"""
	Makes a string to be printed from a list of transfers.

	Parameters
	----------
	transfers : List
		a list of TransferInfo objects

	Returns
	-------
	str
		a string containing information about all transfers in
		the list
	"""
	ret = str(len(transfers)) + " transfers: ["
	for f in transfers:
		ret += ("(from " + str(f.source.id) + " to " + str(f.dest.id) +
			" of file " + str(f.file.id) + ")")
		if f != transfers[len(transfers)-1]:
			ret += ", "
	ret += "]"
	return ret


def print_transfer_list(transfers):
	"""
	Makes a string to be printed from a list of transfers using the
	transfer_list_mkstring function and then prints the string.

	Parameters
	----------
	transfers : List
		a list of TransferInfo objects
	"""
	print(transfer_list_mkstring(transfers))
