import heapq


class EventInfo:
	"""
	A generic class for events that are defined by their types.

	...

	Attributes
	----------
	type : int
		The type of this event.
		0 is running a task in a node;
		1 is redoing the replication decisions;
		2 is the transfer of a file between two nodes of the
		cluster;
		3 is the execution of an external job;
		4 is to reset all metrics involved in the score.

	Methods
	-------
	print()
		Returns a string with information (to be printed) about
		this event.
	"""
	def __init__(self, type):
		"""
		Parameters
		----------
		type : int
			The type of this event.
		"""
		self.type = type

	def print(self):
		"""
		Returns a string with information (to be printed) about
		this event.
		"""
		return "{type: "+str(self.type)+"}"

	def __lt__(self, other):
		"""
		This is the < comparator, used to break ties in the
		events heap (how to choose the order between events
		with the same timestamp?).
		We give priority to transfers

		Parameters
		----------
		other : EventInfo
			other object of the same class to be compared
			with this one.

		Returns
		-------
		bool
			Is this object smaller than the other object?
		"""
		if self.type == 2:
			return False
		elif other.type == 2:
			return True
		else:
			return self.type < other.type


class TaskRun(EventInfo):
	"""
	Event type for the execution of a task (type 0).

	...

	Attributes
	----------
	task : Task
		the task that is being executed in this event
	node : Node
		where the task is running
	"""
	def __init__(self, task, node):
		"""
		Parameters
		----------
		task : Task
			the task that is being executed in this event
		node : Node
			where the task is running
		"""
		EventInfo.__init__(self, 0)
		self.task = task
		self.node = node

	def print(self):
		return ("{type: " +
			str(self.type) +
			", task to file: " +
			str(self.task.file.id) +
			", node: " +
			str(self.node.id) +
			", duration: " +
			str(self.task.duration) +
			", local: " +
			str(self.task.local) +
			"}")


class LocalTransfer(EventInfo):
	"""
	Event type for a transfer of a file between two processing
	nodes (type 2)

	...

	Attributes
	----------
	transfer : TransferInfo
		TransferInfo object with source, destination and file
	duration : int
		how long will this transfer take
	version : int
		an identifier of the event of type 1 (redo all replica
		decisions) that created this transfer. If this transfer
		finishes after the next event of type 1, it will be
		ignored.
	"""
	def __init__(self, transfer, duration, version):
		"""
		Parameters
		----------
		transfer : TransferInfo
			TransferInfo object with source, destination and file
		duration : int
			how long will this transfer take
		version : int
			an identifier of the event of type 1 (redo all replica
			decisions) that created this transfer. If this transfer
			finishes after the next event of type 1, it will be
			ignored.
		"""
		EventInfo.__init__(self, 2)
		self.transfer = transfer
		self.duration = duration
		self.version = version

	def print(self):
		return ("{type: " + str(self.type) +
			", source: " + str(self.transfer.source.id) +
			", dest: " + str(self.transfer.dest.id) +
			", duration: " + str(self.duration) +
			", file: " + str(self.transfer.file.id) +
			", version: " + str(self.version) + "}")


class ExternalJob(EventInfo):
	"""
	Event type for the execution of an external job in a processing
	node (type 3).

	...

	Attributes
	----------
	node : Node
		the node where the external job will run
	"""
	def __init__(self, node):
		"""
		Parameters
		----------
		node : Node
			the node where the external job will run
		"""
		EventInfo.__init__(self, 3)
		self.node = node

	def print(self):
		return ("{type: " + str(self.type) +
			", node: " + str(self.node.id)+"}")


class EventList:
	"""
	A heap of events of different types.

	...

	Attributes
	----------
	events : List
		a list of (time, event) pairs made into a heap using
		the heapq library.
	clock : Float
		current simulation time, advanced when we pop an event.

	Methods
	-------
	push(duration,event)
		schedule an event to the current clock + duration
	pop()
		returns the next event
	"""
	def __init__(self):
		self.events = []
		# made the list into a heap (in place)
		heapq.heapify(self.events)
		self.clock = 0.0

	def push(self, duration, event):
		"""
		Create an event that will take place after the provided
		duration.

		Parameters
		----------
		duration : Double
			in how much time the event should be popped
		event : EventInfo
			object from EventInfo or child class with the
			event
		"""
		heapq.heappush(self.events, (self.clock+duration, event))

	def pop(self):
		"""
		Remove from the list and return the next event, and
		advance the simulation time to when it happens.

		Returns
		-------
		EventInfo
			the event object previously provided to push
		"""
		self.clock, event = heapq.heappop(self.events)
		return event
